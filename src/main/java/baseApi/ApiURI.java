package baseApi;

import static io.restassured.RestAssured.given;


public class ApiURI {

	protected static String URI;

	public String baseUri(int status, String cep) {
		URI = "https://viacep.com.br/ws/"+cep+"/json/";
		given()
		.relaxedHTTPSValidation()
			.get(URI)
		.then()
			.statusCode(status);
		
		return URI;
	}
	
	public String ceps() {
		String ceps= "[94085-170, 94175-000]";
		return ceps;	
	}
	
	public String logradouros() {
		String logradouros = "[Rua Ari Barroso, Rua Almirante Barroso]";
		return logradouros;	
	}
	
	public String bairros() {
		String bairros = "[Morada do Vale I, Recanto Corcunda]";
		return bairros;	
	}
	
	public String localidades() {
		String localidades = "[Gravata�, Gravata�]";
		return localidades;	
	}
	
	public String ufs() {
		String ufs = "[RS, RS]";
		return ufs;	
	}
	
	public String ibge() {
		String ibge = "[4309209, 4309209]";
		return ibge;	
	}

}
