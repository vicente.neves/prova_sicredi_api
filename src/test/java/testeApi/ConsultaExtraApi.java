package testeApi;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import baseApi.ApiURI;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultaExtraApi extends ApiURI {

	private String address = "RS/Gravatai/Barroso"; 
	
	@Test
	public void Step01ValidarUri() {
		
		baseUri(200, address);
	}

	@Test
	public void Step02ConsultaValida() {
		Response response =
		given()
			.get(URI)
		.then()
		.contentType(ContentType.JSON)
		.extract().response();
		
		final String cep = response.jsonPath().getString("cep");
		final String logradouro = response.jsonPath().getString("logradouro");
		final String bairro = response.jsonPath().getString("bairro");
		final String localidade = response.jsonPath().getString("localidade");
		final String uf = response.jsonPath().getString("uf");
		final String ibge = response.jsonPath().getString("ibge");

		assertEquals(ceps(), cep);
		assertEquals(logradouros(), logradouro);
		assertEquals(bairros(), bairro);
		assertEquals(localidades(), localidade);
		assertEquals(ufs(), uf);
		assertEquals(ibge(), ibge);
	}
	
	
}
