package testeApi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import baseApi.ApiURI;
import io.restassured.http.ContentType;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultaInexistenteApi extends ApiURI {

	@Test
	public void Step01ValidarUri() {
		baseUri(200, "12345678");
	}

	@Test
	public void Step02ConsultaValida() {
		given()
			.get(URI)
		.then()
		.contentType(ContentType.JSON)
			.assertThat()
				.body("erro", equalTo(true))
		.extract().response();

	}
	
}
