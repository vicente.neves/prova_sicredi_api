package testeApi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import baseApi.ApiURI;
import io.restassured.http.ContentType;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultaApi extends ApiURI {

	@Test
	public void Step01ValidarUri() {
		baseUri(200, "91060900");
	}

	@Test
	public void Step02ConsultaValida() {
		given()
			.get(URI)
		.then()
		.contentType(ContentType.JSON)
			.assertThat()
				.body("cep", equalTo("91060-900"))
				.body("logradouro", equalTo("Avenida Assis Brasil 3940"))
				.body("complemento", equalTo(""))
				.body("bairro", equalTo("S�o Sebasti�o"))
				.body("localidade", equalTo("Porto Alegre"))
				.body("uf", equalTo("RS"))
				.body("ibge", equalTo("4314902"))
		.extract().response();

	}
	
}
