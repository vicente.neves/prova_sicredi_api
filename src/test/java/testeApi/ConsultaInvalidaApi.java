package testeApi;

import static io.restassured.RestAssured.given;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import baseApi.ApiURI;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ConsultaInvalidaApi extends ApiURI {

	@Test
	public void Step01ValidarUri() {
		baseUri(400, "1234567");
	}

	@Test
	public void Step02ConsultaValida() {
		given()
			.get(URI)
		.then()
		.extract().response();

	}
	
}
